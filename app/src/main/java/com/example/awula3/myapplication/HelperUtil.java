package com.example.awula3.myapplication;

import android.content.Context;
import android.net.ConnectivityManager;

public class HelperUtil {

    public static <T> String getTAG(Class<T> clazz) {
        String className = clazz.getName();
        className = getClassNameFromPackageName(className);
        return className;
    }


    private static String getClassNameFromPackageName(String className) {
        if (className.contains(".")) {
            String[] classNameArr = className.split("\\.");
            className = classNameArr[classNameArr.length - 1];
        }
        return className;
    }

    /**
     * Check if device is online or not
     *
     * @return true if it has access to Internet, false otherwise
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm != null && cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

}
