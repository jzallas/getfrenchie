package com.example.awula3.myapplication.commons;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;

import com.example.awula3.myapplication.HelperUtil;

public class BaseFragment extends Fragment {


    /**
     * Replace the current fragment with the selected fragment. Add it to a backstack if the
     * backstack is specified.
     *
     * @param fragment      - Fragment to show
     * @param backStackName - Add it to the backstack
     */
    public void replaceFragment(Fragment fragment, String backStackName, int containerId) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(containerId, fragment);
        if (backStackName != null) {
            ft.addToBackStack(backStackName);
        }
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        String tag = HelperUtil.getTAG(this.getClass());
        Log.v("On Create", "On Create ---------->  " + tag + " <----------  ");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        String tag = HelperUtil.getTAG(this.getClass());
        Log.v("On Destroy", "On Destroy ---------->  " + tag + " <----------  ");
        super.onDestroy();
    }


}
