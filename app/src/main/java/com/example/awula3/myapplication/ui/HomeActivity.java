package com.example.awula3.myapplication.ui;

import android.os.Bundle;

import com.example.awula3.myapplication.R;
import com.example.awula3.myapplication.commons.BaseActivity;

public class HomeActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(getContainerId(), new HomeFragment())
                    .commit();
        }
    }
    private int getContainerId() {
        return R.id.container;
    }

}
